export function formatDuration(milliseconds, format = "long") {
	if (format === "shorter" && milliseconds > (3.6e6 / 100)) {
		const hours = milliseconds / 3.6e6;
		const roundedHours = Math.round(hours * 100) / 100;
		return `${roundedHours}h`;
	}

	const short = format == "short";
	const seconds = Math.floor(milliseconds / 1000);
	const minutes = Math.floor(seconds / 60);
	const hours = Math.floor(minutes / 60);

	return [
		hours && `${hours}h`,
		((hours && !short) || minutes) && `${minutes % 60}m`,
		(!short) && `${seconds % 60}s`,
	].filter(Boolean).join(" ")
}
