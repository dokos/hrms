import { createResource } from "frappe-ui"
import { employeeResource } from "./employee"

export const expenseSummary = createResource({
	url: "hrms.api.get_expense_summary",
	params: {
		employee: employeeResource.data.name,
	},
	auto: true,
	cache: "hrms:expense_summary",
})