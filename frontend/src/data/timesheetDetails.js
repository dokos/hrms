import { createResource } from "frappe-ui";
import { ref, watch } from "vue";

export const DATE_RANGE_OPTIONS = ["This Week", "Last Week", "This Month"]; // Don't translate this

export const dateRange = ref("This Week");

export const timesheetDetails = createResource({
	auto: false,
	method: "GET",
	url: "/api/method/erpnext.projects.doctype.timesheet.timesheet.get_timesheet_details",
	transform(timesheets) {
		return timesheets.data;
	},
	makeParams() {
		return {
			range: dateRange.value.toLowerCase(),
		}
	},
});

watch(() => dateRange.value, () => timesheetDetails.fetch());
