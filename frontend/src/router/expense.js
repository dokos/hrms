const routes = [
	{
		name: "NewExpense",
		path: "/dashboard/expense/new",
		component: () => import("@/views/expense/Form.vue"),
	},
	{
		name: "ExpenseDetailView",
		path: "/dashboard/expense/:id",
		props: true,
		component: () => import("@/views/expense/Form.vue"),
	},
]

export default routes
