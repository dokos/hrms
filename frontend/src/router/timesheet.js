const routes = [
	{
		name: "ScanFormView",
		path: "/scan/new",
		component: () => import("@/views/scan/Form.vue"),
	},
	{
		name: "TimesheetDetailView",
		path: "/scan/:id",
		component: () => import("@/views/scan/Form.vue"),
		props: true,
	},
]

export default routes
