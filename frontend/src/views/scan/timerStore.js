// @ts-check
import { employeeResource } from "@/data/employee";
import { timesheetDetails } from "@/data/timesheetDetails";
import { useLocalStorage } from "@vueuse/core";
import { createResource, frappeRequest, toast } from "frappe-ui";
import { __ } from "../../plugins/translationsPlugin";

export const timerStore = useLocalStorage("timerStore", [])


// @TODO
const start = new Date();
start.setDate(1);
start.setHours(0)
start.setMinutes(0)
start.setSeconds(0)
const end = new Date("9999-01-01 00:00:00Z");


export const timeLogsListResource = createResource({
	auto: true,
	url: 'erpnext.projects.doctype.timesheet.timesheet.get_time_logs',
	method: 'GET',
	params: {
		start: start.toISOString(),
		end: end.toISOString(),
	},
	transform(timeLogs) {
		return timeLogs.data;
	},
});

export function getListOfTimers() {
	return timerStore.value
}

export function getListOfTimeLogs() {
	return timeLogsListResource.data
}


function generateTimerId() {
	return `@timer:${Date.now()}-${Math.random().toString(36).substr(2, 9)}`;
}

export function makeNewTimer(data = {}) {
	return {
		...data,
		from_time: ISOLocaleString(new Date()),
		to_time: "",
		timerId: generateTimerId(),
	}
}


export function ISOLocaleString(d) {
	const pad = n => n < 10 ? '0' + n : n;
	return d.getFullYear() + '-'
		+ pad(d.getMonth() + 1) + '-'
		+ pad(d.getDate()) + ' '
		+ pad(d.getHours()) + ':'
		+ pad(d.getMinutes()) + ':'
		+ pad(d.getSeconds());
}

/**
 * @param {object} timeLog
 * @returns {Promise<undefined>}
 */
export async function upsertTimeLog(timeLog) {
	await employeeResource.promise

	if (isTimer(timeLog) && timeLog.to_time) {
		await upsertTimeLog({ ...timeLog, timerId: null })
		const index = timerStore.value.findIndex(timer => timer.timerId === timeLog.timerId);
		timerStore.value.splice(index, 1)
	} else if (isTimer(timeLog)) {
		const existingTimer = timerStore.value.find(timer => timer.timerId === timeLog.timerId);
		if (existingTimer) {
			Object.assign(existingTimer, timeLog);
		} else {
			timerStore.value.push(timeLog);
		}
	} else if (!timeLog.to_time) {
		const timer = makeNewTimer()
		Object.assign(timer, timeLog)
		timerStore.value.push(timer);
		showTimerStartedToast()
	} else {
		const data = {
			...timeLog,
			doctype: "Timesheet Detail",
		};
		data.employee ??= employeeResource.data?.name;

		if (!timeLog.activity_type) {
			throw new Error(__("Please select an activity type."));
		}
		if (!timeLog.to_time) {
			throw new Error("Please set an end time or start the timer.");
		}

		const method = "erpnext.projects.doctype.timesheet.timesheet.add_time_log";

		let employees = new Set();
		employees.add(data.employee);
		for (const employee of (data.employees ?? []).filter(Boolean)) {
			employees.add(employee);
		}
		delete data.employee;
		delete data.employees;

		for (const employee of Array.from(employees)) {
			data.employee = employee
			await frappeRequest({
				method: "POST",
				url: "/api/method/" + method,
				params: { values: JSON.stringify(data) },
			});
		}

		timeLogsListResource.reload();
		timesheetDetails.reload();
	}
}

/**
 * @param {string} id
 * @returns {Promise<object>}
 */
export async function getTimeLog(id) {
	if (isTimerId(id)) {
		const timer = timerStore.value.find(timer => timer.timerId === id);
		if (!timer) {
			throw new Error("no such timer")
		}
		return timer
	} else {
		const list = await frappeRequest({
			method: "POST",
			url: "/api/method/" + "erpnext.projects.doctype.timesheet.timesheet.get_time_logs",
			params: {
				start: "0000-01-01 00:00:00Z",
				end: "9999-01-01 00:00:00Z",
				filters: JSON.stringify([["Timesheet Detail", "name", "=", id]]),
			},
		});

		return list[0]
	}
}

/**
 * @param {string} id
 */
export function deleteTimer(id) {
	const index = timerStore.value.findIndex(timer => timer.timerId === id);
	if (index === -1) {
		throw new Error("no such timer")
	}
	const spliced = timerStore.value.splice(index, 1);
	const removed = spliced[0];

	if (removed && isTimer(removed)) {
		toast({
			position: "bottom-center",
			innerHTML: `<div class="flex items-center text-sm gap-2"><span>${__("Timer deleted", null, "HRMS Scan")}</span> <button class="text-white bg-gray-900 px-2 py-1.5 rounded ml-auto">${__("Restore")}</button></div>`,
			onClick(event) {
				event.currentTarget?.classList.add("hidden");
				delete removed.timerId;
				delete removed.to_time;
				upsertTimeLog(removed);
			},
		})
	}
}

export function isTimer(timeLog) {
	if (timeLog.timerId) {
		return true
	}
	return false
}

export function isTimerId(id) {
	if (id.startsWith("@timer:")) {
		return true
	}
	return false
}


/**
 * @param {{ docstatus?: number; }} doc
 */
export function isReadOnly(doc) {
	return (doc.docstatus || 0) !== 0;
}

export function showTimerStartedToast() {
	toast({
		title: __("Timer started", null, "HRMS Scan"),
		icon: "check-circle",
		position: "bottom-center",
		iconClasses: "text-green-500",
	})
}
