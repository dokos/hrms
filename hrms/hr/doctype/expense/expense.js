// Copyright (c) 2023, Frappe Technologies Pvt. Ltd. and contributors
// For license information, please see license.txt

frappe.provide("erpnext.accounts.dimensions");

frappe.ui.form.on("Expense", {
	onload(frm) {
		frm.set_query("project", function (doc) {
			return {
				query: "erpnext.controllers.queries.get_project_name",
				filters: {
					company: doc.company,
				},
			};
		});

		erpnext.accounts.dimensions.setup_dimension_filters(frm, frm.doctype);
	},
	refresh(frm) {
		if (!frm.doc.receipt) {
			frm.trigger("show_file_uploader");
		}

		if (frm.doc.docstatus == 1) {
			frm.add_custom_button(__("Create an expense claim"), () => {
				frappe.model.open_mapped_doc({
					method: "hrms.hr.doctype.expense.expense.make_expense_claim",
					frm: frm,
					freeze_message: __("Creating Expense Claim ...")
				})
			})
		}

		frm.trigger("show_file_preview");

		if (!frm.doc.employee && frappe.boot.user.employee) {
			frm.set_value("employee", frappe.boot.user.employee)
		}

		frm.trigger("recalc");
	},

	show_file_uploader(frm) {
		new frappe.ui.FileUploader({
			folder: frappe.boot.attachments_folder,
			doctype: frm.doctype,
			docname: frm.doc.name,
			frm: frm,
			fieldname: "receipt_html",
			wrapper: frm.get_field("receipt_html").$wrapper,
			make_attachments_public: false,
			restrictions: {
				allowed_file_types: ["image/*", "application/pdf"],
			},
			allow_multiple: false,
			disable_file_browser: true,
			allow_web_link: false,
			on_success(file_doc, response) {
				frm.set_value("receipt", file_doc.file_url);
				frm.trigger("show_file_preview");
			},
		});
	},

	show_file_preview(frm) {
		for (const fn of ["receipt_preview_desktop", "receipt_preview_mobile"]) {
			const $el = frm.get_field(fn)?.$wrapper;
			if ($el) {
				$el.html("");
			}
		}
		if (frm.doc.receipt) {
			frm.events.preview_file(frm, frm.doc.receipt);
		}
	},

	async preview_file(frm, file_url) {
		if (typeof file_url != "string" || !file_url) {
			return;
		} else if (!file_url.includes("/")) {
			const res = await frappe.db.get_value("File", file_url, "file_url");
			file_url = res?.message?.file_url;
			if (file_url?.includes?.("/")) {
				if (frm.doc.docstatus == 0) {
					frm.set_value("receipt", file_url);
				}
				return frm.events.preview_file(frm, file_url);
			}
		}

		let $preview = "";

		if (frappe.utils.is_image_file(file_url)) {
			$preview = $(`<div class="img_preview">
				<img
					class="img-responsive"
					src="${frappe.utils.escape_html(file_url)}"
					onerror="${frm.toggle_display("receipt_preview", false)}"
				/>
			</div>`);
		} else if (frappe.utils.is_video_file(file_url)) {
			$preview = $(`<div class="img_preview">
				<video width="480" height="320" controls>
					<source src="${frappe.utils.escape_html(file_url)}">
					${__("Your browser does not support the video element.")}
				</video>
			</div>`);
		} else if (file_url.endsWith("pdf")) {
			$preview = $(`<div class="img_preview">
				<object style="background:#323639;" width="100%">
					<embed
						style="background:#323639;"
						width="100%"
						height="1190"
						src="${frappe.utils.escape_html(file_url)}" type="application/pdf"
					>
				</object>
			</div>`);
		} else if (file_url.endsWith("mp3")) {
			$preview = $(`<div class="img_preview">
				<audio width="480" height="60" controls>
					<source src="${frappe.utils.escape_html(file_url)}" type="audio/mpeg">
					${__("Your browser does not support the audio element.")}
				</audio >
			</div>`);
		}

		if ($preview) {
			$preview.prepend(`<div class="control-label">${__("Preview")}</div>`);

			for (const fn of ["receipt_preview_desktop", "receipt_preview_mobile"]) {
				frm.toggle_display(fn, true);
				const $el = frm.get_field(fn)?.$wrapper;
				if ($el) {
					$el.html($preview.clone());
				}
			}
		}
	},

	async calculate_total(frm) {
		frm.doc.expense_details.map(d => {
			tax_rate = (d.tax_rate || 0) / 100.0;
			net_amount = d.amount / (1 + tax_rate) || 0.0;
			tax_amount = (d.amount / (1 + tax_rate)) * tax_rate || 0.0;
			frappe.model.set_value(d.doctype, d.name, "net_amount", net_amount)
			frappe.model.set_value(d.doctype, d.name, "tax_amount", tax_amount)
		})

		frm.set_value("grand_total", frm.doc.expense_details.reduce((acc, row) => acc + row.amount, 0)); // TTC
		frm.set_value("net_amount", frm.doc.expense_details.reduce((acc, row) => acc + row.net_amount, 0)); // TTC
		frm.set_value("tax_amount", frm.doc.expense_details.reduce((acc, row) => acc + row.tax_amount, 0)); // TTC
	},

	company(frm) {
		erpnext.accounts.dimensions.update_dimension(frm, frm.doctype);
	}
});

async function get_vat_rate(frm, expense_type) {
	if (!expense_type) return;
	const { message } = await frappe.call("hrms.hr.doctype.expense.expense.get_vat_rate", {
		expense_type,
		company: frm.doc.company,
	});
	return message.tax_rate;
}

async function update_row(frm, cdt, cdn) {
	const row = frappe.get_doc(cdt, cdn);
	const tax_rate = await get_vat_rate(frm, row.expense_type);
	frappe.model.set_value(cdt, cdn, "tax_rate", tax_rate || 0);
	frm.trigger("calculate_total");
}

frappe.ui.form.on("Expense Tax Row", {
	expense_details_add(frm, cdt, cdn) {
		frm.trigger("calculate_total");
	},

	expense_details_remove(frm, cdt, cdn) {
		frm.trigger("calculate_total");
	},

	expense_type(frm, cdt, cdn) {
		update_row(frm, cdt, cdn);
	},

	amount(frm, cdt, cdn) {
		update_row(frm, cdt, cdn);
	},
});
