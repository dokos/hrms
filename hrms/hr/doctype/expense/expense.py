# Copyright (c) 2023, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt

import frappe
from frappe import _
from frappe.model.document import Document
from frappe.model.mapper import get_mapped_doc
from frappe.utils import flt


class Expense(Document):
	def validate(self):
		self.set_status(False)
		self.validate_tax_rates()
		self.calculate_totals()
		self.validate_cost_center()
		self.validate_project()

	def set_status(self, commit=True):
		if self.docstatus.is_draft():
			status = "Draft"
		elif self.docstatus.is_submitted():
			status = "Unclaimed"
			if frappe.db.get_value("Expense Claim Detail", dict(expense=self.name, docstatus=1)):
				status = "Claimed"
		elif self.docstatus.is_cancelled():
			status = "Cancelled"

		if commit and self.status != status:
			self.db_set("status", status)
		self.status = status

	def validate_tax_rates(self):
		for expense in self.expense_details:
			tax_rate = get_vat_rate(expense.expense_type, self.company)
			expense.tax_rate = tax_rate.get("tax_rate", 0.0)

	@frappe.whitelist()
	def calculate_totals(self):
		self.grand_total, self.net_amount, self.tax_amount = 0, 0, 0
		for row in self.expense_details:
			row.amount = flt(row.amount)
			tax_rate = flt(row.tax_rate) / 100.0
			row.net_amount = row.amount / (1 + tax_rate)
			row.tax_amount = (row.amount / (1 + tax_rate)) * tax_rate

			self.grand_total += row.amount
			self.net_amount += row.net_amount
			self.tax_amount += row.tax_amount

	def on_cancel(self):
		self.set_status(False)

	def validate_cost_center(self):
		if not self.cost_center:
			return

		cost_center_company = frappe.get_cached_value("Cost Center", self.cost_center, "company")
		if cost_center_company != self.company:
			frappe.throw(
				_("Cost Center {1} does not belong to company {2}").format(
					frappe.bold(self.idx), frappe.bold(self.cost_center), frappe.bold(self.company)
				)
			)

	def validate_project(self):
		if not self.project:
			return

		project_company = frappe.get_cached_value("Project", self.project, "company")
		if project_company and project_company != self.company:
			frappe.throw(
				_("Project {1} does not belong to company {2}").format(
					frappe.bold(self.idx), frappe.bold(self.project), frappe.bold(self.company)
				)
			)


@frappe.whitelist()
def make_expense_claim(source_name, target_doc=None):
	def update_item(obj, target, source_parent):
		target.description = source_parent.description
		target.project = source_parent.project
		target.cost_center = source_parent.cost_center

	doc = get_mapped_doc(
		"Expense",
		source_name,
		{
			"Expense": {
				"doctype": "Expense Claim",
				"validation": {
					"docstatus": ["=", 1],
				},
			},
			"Expense Tax Row": {
				"doctype": "Expense Claim Detail",
				"field_map": {
					"sanctioned_amount": "amount",
				},
				"postprocess": update_item,
			},
		},
		target_doc,
	)

	return doc


@frappe.whitelist()
def get_expenses(selected_expenses):
	expenses = []
	for exp in frappe.parse_json(selected_expenses):
		exp_doc = frappe.get_doc("Expense", exp)
		exp_dict = exp_doc.as_dict(no_default_fields=True)
		exp_dict["expense"] = exp_doc.name
		expenses.append(exp_dict)

	return expenses


@frappe.whitelist()
@frappe.validate_and_sanitize_search_inputs
def get_expenses_for_expense_claims(doctype, txt, searchfield, start, page_len, filters):
	expense = frappe.qb.DocType("Expense")
	expense_claim_detail = frappe.qb.DocType("Expense Claim Detail")

	query = (
		frappe.qb.from_(expense)
		.left_join(expense_claim_detail)
		.on(expense.name == expense_claim_detail.expense)
		.select(
			expense.name, expense.expense_date, expense.grand_total, expense.cost_center, expense.project, expense.company
		)
		.where(expense.docstatus == 1)
		.where(expense_claim_detail.expense.isnull())
	)

	if "company" in filters:
		query = query.where(expense.company == filters.get("company"))

	expenses = query.run(as_dict=True)

	return expenses


def on_file_deletion(doc, method=None):
	if doc.attached_to_doctype == "Expense" and doc.attached_to_name:
		if doc.name == frappe.db.get_value("Expense", doc.attached_to_name, "receipt"):
			doc = frappe.get_doc("Expense", doc.attached_to_name)
			doc.receipt = None
			doc.amount = None
			doc.expense_type = None
			doc.flags.ignore_mandatory = True
			doc.save()


@frappe.whitelist()
def get_vat_rate(expense_type, company):
	ect = frappe.get_doc("Expense Claim Type", expense_type)
	for acc in ect.accounts:
		if acc.company == company and acc.tax_template:
			ptt = frappe.get_doc("Purchase Taxes and Charges Template", acc.tax_template)
			if taxes := ptt.get("taxes"):
				return {"tax_rate": taxes[0].rate}
	return {}
