frappe.listview_settings["Expense Claim"] = {
	add_fields: ["company"],
	onload(listview) {
		// @dokos
		listview.page.add_inner_button(__("Expense Claim Type"), function () {
			frappe.set_route("List", "Expense Claim Type", {
				name: listview.get_filter_value("expense_type"),
			});
		});
	},
};
