import frappe


def execute():
	for ws in frappe.get_all("Workspace", filters={"module": ("in", ["HR", "Payroll"]), "is_standard": 1}):
		frappe.delete_doc("Workspace", ws.name)
