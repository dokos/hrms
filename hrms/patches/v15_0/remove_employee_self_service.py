import frappe

from frappe.core.page.permission_manager.permission_manager import reset

def execute():
	if frappe.db.exists("User", dict(user_type="Employee Self Service")):
		return

	doctypes = set()
	for custom_docperm in frappe.get_all("Custom DocPerm", filters={"role": "Employee Self Service"}, fields=["parent", "name"]):
		frappe.db.set_value("Custom DocPerm", custom_docperm.name, "role", "Employee")
		doctypes.add(custom_docperm.parent)

	for dt in doctypes:
		if all(x=="Administrator" for x in frappe.get_all("Custom DocPerm", filters={"parent": dt}, pluck="owner")):
			reset(dt)

	frappe.delete_doc("User Type", "Employee Self Service")