import frappe

from hrms.regional.france.setup import hide_payroll_workspace


def execute():
	company = frappe.get_all("Company", filters={"country": "France"})
	if not company:
		return

	hide_payroll_workspace()