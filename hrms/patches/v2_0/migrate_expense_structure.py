import frappe
import erpnext

from hrms.hr.doctype.expense.expense import get_vat_rate

def execute():
	for expense in frappe.get_all("Expense", filters={"net_amount": 0.0, "docstatus": (">", 0)}, fields=["name", "company"]):
		if not expense.company:
			expense.company = erpnext.get_default_company()
			frappe.db.set_value("Expense", expense.name, "company", expense.company)

		tax_row = frappe.new_doc("Expense Tax Row")
		tax_row.parenttype = "Expense"
		tax_row.parent = expense.name
		tax_row.parentfield = "expense_details"
		tax_row.expense_type = frappe.db.get_value("Expense", expense.name, "expense_type")
		tax_row.amount = frappe.db.get_value("Expense", expense.name, "amount")
		tax_row.tax_rate = get_vat_rate(tax_row.expense_type, expense.company).get("tax_rate", 0.0)
		tax_rate = (tax_row.tax_rate or 0.0) / 100.0
		tax_row.tax_amount = (tax_row.amount / (1 + tax_rate)) * tax_rate
		tax_row.net_amount = tax_row.amount / (1 + tax_rate)
		tax_row.insert()

		doc = frappe.get_doc("Expense", expense.name)
		doc.calculate_totals()

		frappe.db.set_value("Expense", expense.name, "net_amount", doc.net_amount)
		frappe.db.set_value("Expense", expense.name, "grand_total", doc.amount)
		frappe.db.set_value("Expense", expense.name, "tax_amount", doc.tax_amount)