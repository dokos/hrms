import frappe
from frappe import _


def setup_default_expense_types(company):
	if not company:
		return

	# Remove default expense types
	std_expense_types = [_("Calls"), _("Food"), _("Medical"), _("Others"), _("Travel")]
	if frappe.db.count("Expense Claim Type") == len(std_expense_types):
		if all(frappe.db.exists("Expense Claim Type", name) for name in std_expense_types):
			for name in std_expense_types:
				frappe.delete_doc("Expense Claim Type", name)

	def make_ect(name: str, account="", vat="", **kwargs):
		"""
		name (str): Le nom français du type de note de frais
		vat (str): Le préfixe du nom du modèle de taxe (auquel on ajoute l'abbréviation de la société)
		account (str): Idem pour le compte comptable
		"""
		values = {
			"deferred_expense_account": 0,
			"expense_type": name,
		}
		if docname := frappe.db.exists("Expense Claim Type", values):
			doc = frappe.get_doc("Expense Claim Type", docname)
		else:
			doc = frappe.new_doc("Expense Claim Type")
			doc.update(values)
			doc.update(kwargs)

		account = account and frappe.db.exists("Account", {"title": account, "company": company})
		vat = vat and frappe.db.exists(
			"Purchase Taxes and Charges Template", {"title": vat, "company": company}
		)

		if account:
			row = {"company": company, "default_account": account}
			if vat:
				row["tax_template"] = vat
			doc.append("accounts", row)

		doc.save()

	NON_DEDUCTIBLE = ""  # TVA non déductible = Pas de lignes de taxe

	make_ect("Régularisation frais kilométriques", "6251 - Voyages et déplacements", NON_DEDUCTIBLE)
	make_ect("Péage", "6251 - Voyages et déplacements", "TVA 20% Déductible")
	make_ect("Parking", "6251 - Voyages et déplacements", NON_DEDUCTIBLE)
	make_ect(
		"Location d'un véhicule de tourisme",
		"6251 - Voyages et déplacements",
		NON_DEDUCTIBLE,
		description="Location d'un véhicule de tourisme auprès d'une société de location.",
	)
	make_ect(
		"Invitations internes (restaurant, réception, spectacle)",
		"6257 - Réceptions",
		"TVA 20% Déductible",
	)
	make_ect(
		"Invitations externes (restaurant, réception, spectacle)",
		"6257 - Réceptions",
		"TVA 20% Déductible",
	)
	make_ect("Frais postaux", "626 - Frais postaux et télécommunications", "TVA 20% Déductible")
	make_ect("Frais d'hébergement au profit de tiers", "6256 - Missions", "TVA 20% Déductible")
	make_ect(
		"Frais d'hébergement au profit de salarié(s) ou dirigeant(s)",
		"6256 - Missions",
		NON_DEDUCTIBLE,
	)
	make_ect(
		"Frais de transport (train, avion, bus, etc.)",
		"6251 - Voyages et déplacements",
		NON_DEDUCTIBLE,
	)
	make_ect("Frais de taxi", "6251 - Voyages et déplacements", NON_DEDUCTIBLE)
	make_ect("Frais de restauration en mission", "6256 - Missions", "TVA 5.5% Déductible")
	make_ect("Documentation générale", "6181 - Documentation générale", "TVA 20% Déductible")
	make_ect("Cadeaux clientèle", "6234 - Cadeaux à la clientèle", "TVA 20% Déductible")
	make_ect("Divers (facture au nom de l'entreprise)", "628 - Divers", "TVA 20% Déductible")
