import frappe
from frappe import _


def setup_default_leaves():
	leave_types = [
		{
			"doctype": "Leave Type",
			"leave_type_name": _("RTT"),
			"name": _("RTT"),
			"allow_encashment": 0,
			"is_carry_forward": 0,
			"include_holiday": 0,
			"is_compensatory": 0,
			"max_leaves_allowed": 8,
			"period_start_day": 1,
			"period_start_month": 1,
			"period_end_day": 31,
			"period_end_month": 12,
		},
		{
			"doctype": "Leave Type",
			"leave_type_name": _("Congés Payés"),
			"name": _("Congés Payés"),
			"allow_encashment": 0,
			"is_carry_forward": 1,
			"include_holiday": 0,
			"is_compensatory": 0,
			"max_leaves_allowed": 30,
			"allow_negative": 1,
			"is_earned_leave": 1,
			"earned_leave_frequency": "Congés payés sur jours ouvrables",
			"period_start_day": 1,
			"period_start_month": 6,
			"period_end_day": 31,
			"period_end_month": 5,
		},
		{
			"doctype": "Leave Type",
			"leave_type_name": "Arrêt Maladie",
			"name": "Arrêt Maladie",
			"allow_negative": 1,
			"is_lwp": 1,
		},
		{
			"doctype": "Leave Type",
			"leave_type_name": "Mariage du salarié",
			"name": "Mariage - salarié",
			"max_leaves_allowed": 4,
			"calculate_in_business_days": 0,  # Can be changed if needed
		},
		{
			"doctype": "Leave Type",
			"leave_type_name": "Mariage d'un enfant",
			"name": "Mariage - enfant",
			"max_leaves_allowed": 1,
			"calculate_in_business_days": 0,  # Can be changed if needed
		},
		{
			"doctype": "Leave Type",
			"leave_type_name": "Naissance ou adoption",
			"name": "Naissance ou adoption",
			"max_leaves_allowed": 3,
		},
		{
			"doctype": "Leave Type",
			"leave_type_name": "Décès: Conjoint, enfant, ascendants",
			"name": "Décès: Conjoint, enfant, ascendants",
			"max_continuous_days_allowed": 2,
			"allow_negative": 1,
		},
		{
			"doctype": "Leave Type",
			"leave_type_name": "Décès: Collatéraux jusqu’au 2eme degré",
			"name": "Décès: Collatéraux jusqu’au 2eme degré",
			"max_continuous_days_allowed": 1,
			"allow_negative": 1,
		},
		{
			"doctype": "Leave Type",
			"leave_type_name": "Décès: Beaux-parents",
			"name": "Décès: Beaux-parents",
			"max_continuous_days_allowed": 1,
			"allow_negative": 1,
		},
	]

	for leave_type in leave_types:
		if frappe.db.exists("Leave Type", leave_type.get("name")):
			doc = frappe.get_doc("Leave Type", leave_type.get("name"))
			if doc.modified_by != "Administrator":
				# Ne pas mettre à jour si modifié par un utilisateur après l'installation.
				print("Leave Type {} has been modified since last update. Skipping.".format(doc.name))
				continue
			doc.update(leave_type)
			doc.flags.ignore_permissions = True
			doc.save()
		else:
			doc = frappe.get_doc(leave_type)
			doc.insert(ignore_permissions=True, ignore_if_duplicate=True)
