# Copyright (c) 2022, Dokos SAS and Contributors
# License: GNU General Public License v3. See license.txt

import frappe
from frappe import _, make_property_setter
from frappe.custom.doctype.custom_field.custom_field import create_custom_fields
from frappe.permissions import setup_custom_perms

from hrms.regional.france.hr.expense_types import setup_default_expense_types
from hrms.regional.france.hr.leave_types import setup_default_leaves


def setup(company=None, patch=True):
	make_property_setters()
	make_custom_fields()
	setup_default_leaves()
	setup_default_expense_types(company)
	setup_document_permissions()
	hide_payroll_workspace()


def make_property_setters():
	property_setters = [
		{
			"doctype": "Leave Type",
			"fieldname": "earned_leave_frequency",
			"property": "options",
			"value": "Monthly\nQuarterly\nHalf-Yearly\nYearly\nCongés payés sur jours ouvrables\nCongés payés sur jours ouvrés",
			"property_type": "Select",
		},
		{
			"doctype": "Leave Type",
			"fieldname": "encashment",
			"property": "hidden",
			"value": "1",
			"property_type": "Check",
		},
		{
			"doctype": "Leave Type",
			"fieldname": "based_on_date_of_joining",
			"property": "depends_on",
			"value": "eval:doc.is_earned_leave&&['Monthly', 'Quarterly', 'Half-Yearly', 'Yearly'].includes(doc.earned_leave_frequency)",
			"property_type": "Code",
		},
		{
			"doctype": "Leave Type",
			"fieldname": "rounding",
			"property": "depends_on",
			"value": "eval:doc.is_earned_leave&&['Monthly', 'Quarterly', 'Half-Yearly', 'Yearly'].includes(doc.earned_leave_frequency)",
			"property_type": "Code",
		},
		{
			"doctype": "HR Settings",
			"fieldname": "auto_leave_encashment",
			"property": "hidden",
			"value": "1",
			"property_type": "Check",
		},
	]
	for property_setter in property_setters:
		make_property_setter(
			property_setter,
			is_system_generated=True,
		)


def make_custom_fields(update=True):
	# Keep for translations:
	__ = _("Calculate attendances")
	__ = _("Allocate Leaves from Contracts")
	__ = _("Attendances for each employee will be calculated and don't need to be explicitely registered")
	__ = _(
		"Leave allocations will automatically attributed based on the rule defined in the employment contract"
	)
	__ = _("If checked the associated leave request will be calculated in business days")
	__ = _("Calculate in Business Days")

	custom_fields = {
		"HR Settings": [
			dict(
				fieldname="calculate_attendances",
				label="Calculate attendances",
				description="Attendances for each employee will be calculated and don't need to be explicitely registered",
				fieldtype="Check",
				insert_after="auto_leave_encashment",
			),
			dict(
				fieldname="allocate_leaves_from_contracts",
				label="Allocate Leaves from Contracts",
				description="Leave allocations will automatically attributed based on the rule defined in the employment contract",
				fieldtype="Check",
				insert_after="calculate_attendances",
			),
		],
		"Leave Type": [
			dict(
				fieldname="calculate_in_business_days",
				label="Calculate in Business Days",
				description="If checked the associated leave request will be calculated in business days",
				fieldtype="Check",
				insert_after="is_compensatory",
			),
		],
	}

	create_custom_fields(custom_fields, ignore_validate=frappe.flags.in_patch, update=update)


def setup_document_permissions():
	setup_custom_perms("Leave Encashment")

	ptypes = [
		field.fieldname for field in frappe.get_meta("Custom DocPerm").fields if field.fieldtype == "Check"
	]
	for permdoc in frappe.get_all("Custom DocPerm", filters={"parent": "Leave Encashment"}):
		doc = frappe.get_doc("Custom DocPerm", permdoc.name)
		for ptype in ptypes:
			doc.set(ptype, 0)

		doc.save()


def hide_payroll_workspace():
	for workspace in frappe.get_all("Workspace", filters={"module": "Payroll"}, pluck="name"):
		frappe.db.set_value("Workspace", workspace, "is_hidden", 1)


def update_regional_tax_settings(country, company):
	pass
