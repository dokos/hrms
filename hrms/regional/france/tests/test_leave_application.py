import frappe

from frappe.tests.utils import FrappeTestCase

from erpnext.setup.doctype.employee.test_employee import make_employee

from hrms.regional.france.tests.utils import setup_tests_for_france
from hrms.hr.doctype.leave_application.leave_application import OverlapError


class TestFranceLeaveApplication(FrappeTestCase):
	@classmethod
	def setUpClass(cls):
		setup_tests_for_france()

	@classmethod
	def tearDownClass(cls):
		frappe.db.rollback()

	def setUp(self):
		self.employee = make_employee("testfranceleavesemp@example.com", company="_Test Company")
		frappe.db.set_value("Employee", self.employee, "holiday_list", f"_Test France Holiday List 2024")

		frappe.db.delete("Leave Ledger Entry")
		frappe.db.delete("Leave Allocation")
		frappe.db.delete("Leave Application")

	def test_leave_application_overlap(self):
		leave_application = make_leave_application(
			employee=self.employee,
			from_date="2024-03-22",
			to_date="2024-04-01",
			leave_type="Congés Payés sur jours ouvrés",
			half_day=False,
			half_day_date=None,
		)
		leave_application.insert()
		leave_application.submit()

		leave_application2 = make_leave_application(
			employee=self.employee,
			from_date="2024-03-29",
			to_date="2024-04-08",
			leave_type="Congés Payés sur jours ouvrés",
			half_day=False,
			half_day_date=None,
		)
		leave_application2.insert()
		leave_application2.submit()

		leave_application3 = make_leave_application(
			employee=self.employee,
			from_date="2024-03-29",
			to_date="2024-04-08",
			leave_type="Congés Payés sur jours ouvrés",
			half_day=False,
			half_day_date=None,
		)
		self.assertRaises(OverlapError, leave_application3.insert)


def make_leave_application(
	employee,
	from_date,
	to_date,
	leave_type,
	half_day=False,
	half_day_date=None,
):
	leave_application = frappe.get_doc(
		dict(
			doctype="Leave Application",
			employee=employee,
			leave_type=leave_type,
			from_date=from_date,
			to_date=to_date,
			half_day=half_day,
			half_day_date=half_day_date,
			company="_Test Company",
			status="Approved",
			leave_approver="test@example.com",
		)
	)
	return leave_application