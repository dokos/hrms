import frappe

from erpnext.setup.doctype.holiday_list.test_holiday_list import make_holiday_list
from hrms.regional.france.hr.bank_holidays import get_french_bank_holidays
from hrms.regional.france.setup import setup


def setup_tests_for_france():
	frappe.flags.country = "France"
	frappe.flags.company = None

	frappe.db.delete("Leave Type")

	setup()

	frappe.db.set_single_value("HR Settings", "allocate_leaves_from_contracts", 1)
	frappe.db.set_single_value("HR Settings", "calculate_attendances", 1)

	leave_types = [
		{
			"doctype": "Leave Type",
			"leave_type_name": "Congés Payés sur jours ouvrables",
			"name": "Congés Payés sur jours ouvrables",
			"allow_encashment": 0,
			"is_carry_forward": 1,
			"include_holiday": 0,
			"is_compensatory": 0,
			"max_leaves_allowed": 30,
			"allow_negative": 1,
			"is_earned_leave": 1,
			"earned_leave_frequency": "Congés payés sur jours ouvrables",
			"period_start_day": 1,
			"period_start_month": 6,
			"period_end_day": 31,
			"period_end_month": 5
		},
		{
			"doctype": "Leave Type",
			"leave_type_name": "Congés Payés sur jours ouvrés",
			"name": "Congés Payés sur jours ouvrés",
			"allow_encashment": 0,
			"is_carry_forward": 1,
			"include_holiday": 0,
			"is_compensatory": 0,
			"max_leaves_allowed": 25,
			"allow_negative": 1,
			"is_earned_leave": 1,
			"earned_leave_frequency": "Congés payés sur jours ouvrés",
			"period_start_day": 1,
			"period_start_month": 6,
			"period_end_day": 31,
			"period_end_month": 5
		},
	]
	for leave_type in leave_types:
			doc = frappe.get_doc(leave_type)
			doc.insert(ignore_permissions=True, ignore_if_duplicate=True)

	last_holiday_list = None
	for year in [2018, 2019, 2020, 2021, 2022, 2023, 2024]:
		bank_holidays = [{"holiday_date": value, "description": key} for key, value in get_french_bank_holidays(year).items()]
		holiday_list = make_holiday_list(
			f"_Test France Holiday List {year}", f"{year}-01-01", f"{year}-12-31", bank_holidays
		)
		holiday_list.weekly_off = "Saturday"
		holiday_list.get_weekly_off_dates()
		holiday_list.weekly_off = "Sunday"
		holiday_list.get_weekly_off_dates()
		holiday_list.replaces_holiday_list = last_holiday_list
		holiday_list.save()

		last_holiday_list = holiday_list.name

	for contract_type in ["Temps plein", "Temps partiel"]:
		frappe.get_doc({
			"doctype": "Employment Contract Type",
			"employment_contract_type": contract_type,
		}).insert(ignore_if_duplicate=True)